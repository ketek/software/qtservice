#####################################################################
## Windows backend plugin:
#####################################################################

qt_internal_add_plugin(WindowsServicePlugin
	OUTPUT_NAME qwindows
	PLUGIN_TYPE servicebackends
	SOURCES
		windowsserviceplugin.cpp windowsserviceplugin.h
		windowsservicebackend.cpp windowsservicebackend.h
		windowsservicecontrol.cpp windowsservicecontrol.h
	LIBRARIES
		Qt::Core
		Qt::Service
)

qt_internal_extend_target(WindowsServicePlugin
	LIBRARIES
		advapi32
)
