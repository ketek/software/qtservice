add_subdirectory(standard)
if(WIN32)
	add_subdirectory(windows)
endif()
if(UNIX)
	pkg_check_modules(SYSTEMD REQUIRED systemd)
	add_subdirectory(systemd)
endif()
