#ifndef QCTRLSIGNALHANDLER_UNIXPRIVATE_H
#define QCTRLSIGNALHANDLER_UNIXPRIVATE_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists purely as an
// implementation detail.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include "qctrlsignalhandler_p.h"

#include <QSocketNotifier>
#include <QObject>

QT_BEGIN_NAMESPACE
class QCtrlSignalHandlerUnixPrivate : public QObject, public QCtrlSignalHandlerPrivate
{
	Q_DISABLE_COPY(QCtrlSignalHandlerUnixPrivate)
	Q_OBJECT

public:
	QCtrlSignalHandlerUnixPrivate(QCtrlSignalHandler *q);

	bool registerSignal(int signal) override;
	bool unregisterSignal(int signal) override;
	void changeAutoQuitMode(bool enabled) override;
	QReadWriteLock *lock() const override;

private Q_SLOTS:
	void socketNotifyTriggerd(int socket);

private:
	QSocketNotifier *socketNotifier = nullptr;

	bool isAutoQuitRegistered(int signal) const;
	bool updateSignalHandler(int signal, bool active);

	static int sockpair[2];
	static const QVector<int> shutSignals;

	static void unixSignalHandler(int signal);
};

QT_END_NAMESPACE
#endif // QCTRLSIGNALHANDLER_UNIXPRIVATE_H
