#ifndef QTSERVICE_SERVICECONTROL_P_H
#define QTSERVICE_SERVICECONTROL_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists purely as an
// implementation detail.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include "qtservice_global.h"
#include "servicecontrol.h"

#include <QtCore/QLoggingCategory>

QT_BEGIN_NAMESPACE
namespace QtService {

class ServiceControlPrivate
{
	Q_DISABLE_COPY(ServiceControlPrivate)

public:
	ServiceControlPrivate(QString &&serviceId);

	QString serviceId;
	QString serviceName;
	bool blocking = true;
	QString error;
};

Q_DECLARE_LOGGING_CATEGORY(logSvcCtrl)

}

QT_END_NAMESPACE
#endif // QTSERVICE_SERVICECONTROL_P_H
