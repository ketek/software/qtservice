#ifndef QTSERVICE_SERVICEBACKEND_P_H
#define QTSERVICE_SERVICEBACKEND_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists purely as an
// implementation detail.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include "servicebackend.h"

#include <QtCore/QLoggingCategory>

QT_BEGIN_NAMESPACE
namespace QtService {

class ServiceBackendPrivate
{
	Q_DISABLE_COPY(ServiceBackendPrivate)
public:
	ServiceBackendPrivate(Service *service);

	Service *service;
	bool operating = false;
};

Q_DECLARE_LOGGING_CATEGORY(logBackend)  // MAJOR make virtual in public part

}

QT_END_NAMESPACE
#endif // QTSERVICE_SERVICEBACKEND_P_H
