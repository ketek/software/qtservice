#ifndef QTSERVICE_TERMINALSERVER_P_H
#define QTSERVICE_TERMINALSERVER_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists purely as an
// implementation detail.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include "terminal.h"
#include "service.h"

#include <QtCore/QObject>
#include <QtCore/QLoggingCategory>

#include <QtNetwork/QLocalServer>

QT_BEGIN_NAMESPACE
namespace QtService {

class TerminalPrivate;
class TerminalServer : public QObject
{
	Q_OBJECT

public:
	explicit TerminalServer(Service *service);

	static QString serverName();

	bool start(bool globally);
	void stop();

	bool isRunning() const;

Q_SIGNALS:
	void terminalConnected(QtService::Terminal *terminal);

private Q_SLOTS:
	void newConnection();

	void terminalReady(TerminalPrivate *terminal, bool success);

private:
	Service *_service;
	QLocalServer *_server;
	bool _activated = false;

	bool setSocketDescriptor(int socket);
};

Q_DECLARE_LOGGING_CATEGORY(logTermServer)

}

QT_END_NAMESPACE
#endif // QTSERVICE_TERMINALSERVER_P_H
