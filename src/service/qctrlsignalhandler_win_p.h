#ifndef QCTRLSIGNALHANDLERPRIVATE_WIN_H
#define QCTRLSIGNALHANDLERPRIVATE_WIN_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists purely as an
// implementation detail.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include "qctrlsignalhandler_p.h"

#include <qt_windows.h>

QT_BEGIN_NAMESPACE
class QCtrlSignalHandlerWinPrivate : public QCtrlSignalHandlerPrivate
{
	Q_DISABLE_COPY(QCtrlSignalHandlerWinPrivate)
public:
	QCtrlSignalHandlerWinPrivate(QCtrlSignalHandler *q);
	~QCtrlSignalHandlerWinPrivate() override;

	bool registerSignal(int signal) override;
	bool unregisterSignal(int signal) override;
	void changeAutoQuitMode(bool) override;

	QReadWriteLock *lock() const override;

private:
	mutable QReadWriteLock rwLock;

	bool handleAutoQuit(DWORD signal);

	static BOOL WINAPI HandlerRoutine(_In_ DWORD dwCtrlType);
};

QT_END_NAMESPACE
#endif // QCTRLSIGNALHANDLERPRIVATE_WIN_H
