#include "qctrlsignalhandler_unix_p.h"
#include <QCoreApplication>
#include <unistd.h>
#include <sys/socket.h>

int QCtrlSignalHandlerUnixPrivate::sockpair[2];
const QVector<int> QCtrlSignalHandlerUnixPrivate::shutSignals = {SIGINT, SIGTERM, SIGQUIT, SIGHUP};

QCtrlSignalHandlerPrivate *QCtrlSignalHandlerPrivate::createInstance(QCtrlSignalHandler *q_ptr)
{
	return new QCtrlSignalHandlerUnixPrivate{q_ptr};
}

QCtrlSignalHandlerUnixPrivate::QCtrlSignalHandlerUnixPrivate(QCtrlSignalHandler *q_ptr) :
	QObject{},
	QCtrlSignalHandlerPrivate{q_ptr}
{
	if(::socketpair(AF_UNIX, SOCK_STREAM, 0, sockpair) == 0) {
		socketNotifier = new QSocketNotifier{sockpair[1], QSocketNotifier::Read, this};
		connect(socketNotifier, &QSocketNotifier::activated,
				this, &QCtrlSignalHandlerUnixPrivate::socketNotifyTriggerd);
		socketNotifier->setEnabled(true);
	} else {
		qCWarning(logQCtrlSignals).noquote() << "Failed to create socket pair with error:"
											 << qt_error_string();
	}
}

bool QCtrlSignalHandlerUnixPrivate::registerSignal(int signal)
{
	if(isAutoQuitRegistered(signal))
		return true;
	else
		return updateSignalHandler(signal, true);
}

bool QCtrlSignalHandlerUnixPrivate::unregisterSignal(int signal)
{
	if(isAutoQuitRegistered(signal))
		return true;
	else
		return updateSignalHandler(signal, false);
}

void QCtrlSignalHandlerUnixPrivate::changeAutoQuitMode(bool enabled)
{
	for(auto sig : shutSignals) {
		if(!activeSignals.contains(sig))
			updateSignalHandler(sig, enabled);
	}
}

QReadWriteLock *QCtrlSignalHandlerUnixPrivate::lock() const
{
	return nullptr;//no locks needed on unix
}

void QCtrlSignalHandlerUnixPrivate::socketNotifyTriggerd(int socket)
{
	int signal;
	if(::read(socket, &signal, sizeof(int)) == sizeof(int)) {
		if(!reportSignalTriggered(signal) &&
		   isAutoQuitRegistered(signal))
			qApp->quit();
	} else
		qCWarning(logQCtrlSignals) << "Failed to read signal from socket pair";
}

bool QCtrlSignalHandlerUnixPrivate::isAutoQuitRegistered(int signal) const
{
	if(autoQuit)
		return shutSignals.contains(signal);
	else
		return false;
}

bool QCtrlSignalHandlerUnixPrivate::updateSignalHandler(int signal, bool active)
{
	struct sigaction action;
	action.sa_handler = active ? QCtrlSignalHandlerUnixPrivate::unixSignalHandler : SIG_DFL;
	sigemptyset(&action.sa_mask);
	action.sa_flags |= SA_RESTART;
	if(::sigaction(signal, &action, nullptr) == 0)
		return true;
	else {
		qCWarning(logQCtrlSignals).noquote() << "Failed to"
											 << (active ? "register" : "unregister")
											 << "signal with error:"
											 << qt_error_string();
		return false;
	}
}

void QCtrlSignalHandlerUnixPrivate::unixSignalHandler(int signal)
{
	const auto wr = ::write(sockpair[0], &signal, sizeof(int));
	Q_UNUSED(wr);
}
